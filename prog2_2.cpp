//
//  main.cpp
//  Assignment2
//
//  Created by Eddie Mun on 11/2/18.
//  Copyright © 2018 Eddie Mun. All rights reserved.
//

#include <iostream>
#include <vector>
#include <iostream>
#include <numeric>
#include <string>
#include <fstream>
#include <algorithm>
#include <sstream>
#include <stdlib.h>
#include "prog2_1.hpp"

using namespace std;

int main(int argc, char** argv) {
    cout << "Assignment #2-2, Edward Mun, eddiejmun@gmail.com" << endl;
    vector<string> textin;
    Tokenizer t;
    bool stop = false;
    //takes the input from command line and converts to string
    string input(argv[1]);
    string word;
    ifstream files;
    //reads in .txt file line by line
    files.open(input);
    while (!files.eof()) {
        getline(files,word);
        textin.push_back(word);
    }
    //converts the .txt file into a string
    files.close();
    string textfile;
    for (const auto &piece : textin) {
        textfile += piece;
        textfile += " ";
    }
    try {
        t.Tokenize(textfile);
    } catch(string s) {
        string error = "Error on line ";
        cout << error << t.npoint << ": " << s << endl;
        stop = true;
        
    }
    //formats the .txt file and replaces spaces with commas
    if (stop == false) {
        for(int j = 0; j < textin.size(); j++) {
            string var = textin[j];
            replace(var.begin(), var.end(), ' ', ',');
            cout << var << endl;
        }
    }
    return 0;
}
