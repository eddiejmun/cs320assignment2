//
//  prog2_1.cpp
//  Assignment2
//
//  Created by Eddie Mun on 11/2/18.
//  Copyright © 2018 Eddie Mun. All rights reserved.
//
#include <vector>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <iostream>
#include <string>
#include "prog2_1.hpp"
using namespace std;


vector<string> tokens;
bool match = true;

Tokenizer::Tokenizer() {}
Tokenizer::~Tokenizer() {}

void Tokenizer::Tokenize(string str) {
    vector<string> tokens;
    int point = 0;
    vector<string> commands{"push","pop","add","sub","mod","skip","save","get","mul","div"};
    stringstream check1(str);
    string line;
    //tokenizes string by spaces and stores into a vector<string>
    while (getline(check1, line, ' ')) {
        tokens.push_back(line);
        
    }
    do{
        match = false;
        //if tokens is one of the commands then it is a match
        for(int i = 0; i < commands.size(); i++) {
            if (tokens[point] == commands[i]) {
                match = true;
            }
        }
        //if token is a valid integer then it is a match
        size_t found = tokens[point].find_first_not_of("0123456789");
        if (found == -1) {
            match = true;
        }
        if (match == false) {
            //finds which line the error is on
            npoint = point + 1;
            if (npoint % 2 == 0) {
                npoint = npoint/2;
            }
            else {
                npoint++;
                npoint = npoint/2;
            }
            throw string("Unexpected token: " + tokens[point]);
        }
        point++;
    }while(match == true && point < tokens.size());
}

vector<string> Tokenizer::GetTokens() {
    //outputs the tokens in the queue
    vector<string> tokenqueue;
    if(match == true) {
        for(int i = 0; i < tokens.size(); i++) {
            tokenqueue[i] = tokens[i];
        }
        for(int i = 0; i < tokenqueue.size(); i++) {
            cout << tokenqueue[i];
        }
        throw "No tokens";
        return tokenqueue;
    }
}
