//
//  prog2_1.hpp
//  Assignment2
//
//  Created by Eddie Mun on 11/2/18.
//  Copyright © 2018 Eddie Mun. All rights reserved.
//

#ifndef prog2_1_hpp
#define prog2_1_hpp
#include <vector>
#include <fstream>
#include <sstream>
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <string>


using namespace std;

class Tokenizer {
public:
    int npoint;
    void Tokenize(string str);
    vector<string> GetTokens();
    Tokenizer();
    ~Tokenizer();
    
};
#endif /* prog2_1_hpp */
